package com.marcolicci;

import java.lang.*;

import static java.lang.Thread.currentThread;

public class Main {

    public static void main(String[] args) {
        //ES 1
        System.out.println(currentThread().getName());
        new Thread1().start();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //ES 2
        System.out.print("\n\n\n\n");
        new Thread2().start();
        new Thread2().start();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //ES 3
        System.out.print("\n\n\n\n");
        for (int i = 1; i < 8; i++) {
            new Thread3("Nano" + i).start();
        }
    }
}
